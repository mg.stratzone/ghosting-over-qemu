#!/usr/bin/env bash

# Vars
SYSDRIVE="${1:-/dev/nvme0n1}"
INAME=$(ls /sys/class/net | grep -i en)
REAL_MAC=$(cat /sys/class/net/${INAME}/address)
BRNAME="br0"
RANDOM_MAC1=($(printf 'FE:EE:C1:%02X:%02X:%02X\n' $[RANDOM%256] $[RANDOM%256] $[RANDOM%256]))
RANDOM_MAC2=($(printf 'FE:EE:C2:%02X:%02X:%02X\n' $[RANDOM%256] $[RANDOM%256] $[RANDOM%256]))

# Create a bridge. Spoof real interface MAC address to VM && assign a random MAC to real interface
ip link set ${INAME} down

ip link add ${BRNAME} type bridge
ip link set ${INAME} master ${BRNAME}

ip link set ${INAME} address ${RANDOM_MAC1}
ip link set ${INAME} up

ip link set ${BRNAME} address ${RANDOM_MAC2}
ip link set ${BRNAME} up

dhclient ${BRNAME}

# Start Ghost VM
mkdir -p /etc/qemu
echo "allow all" | tee /etc/qemu/bridge.conf
chmod 0640 /etc/qemu/bridge.conf

bash -c "
sleep 12
for i in {1..6};
do
    echo sendkey down | nc -N 127.0.0.1 55555
    sleep 0.5
done" &

qemu-system-x86_64 \
    -drive "if=pflash,format=raw,readonly=on,file=/opt/qemu-ressources/OVMF_CODE.fd" \
    -drive "if=pflash,format=raw,readonly=on,file=/opt/qemu-ressources/OVMF_VARS.fd" \
    -enable-kvm \
    -cpu host \
    -m 12G \
    -rtc base=utc \
    -net nic,model=virtio,macaddr=${REAL_MAC} \
    -net bridge,br=${BRNAME},id=net0 \
    -hda ${SYSDRIVE} \
    -vga virtio \
    -cdrom /opt/qemu-ressources/netboot.iso \
    -display sdl,alt_grab=on,window_close=off \
    -monitor tcp:127.0.0.1:55555,server,nowait
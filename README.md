# ghosting_over_qemu

Info: https://wiki.archlinux.org/title/archiso#Installation

## Workflow

- `airootfs/etc/systemd/system/xfce4.service` (Symlinked to `airootfs/etc/systemd/system/multi-user.target.wants/xfce4.service`)
    - Start XFCE at system boot

- `airootfs/root/.config/autostart/ghost-vm.desktop`
    - Execute `start_ghost_vm.sh` script at XFCE launch

- `airootfs/root/start_ghost_vm.sh`  
    - Create a bridge.
    - Spoof real interface MAC address to VM && assign a random MAC to real interface.  
    - Passthrough a physical drive to VM and boot VM on PXE with predefined BIOS settings in `OVMF_VARS.fd`.

- `airootfs/root/OVMF_VARS.fd`
    - ~~Boot Order~~ (Fails when MAC address change):
        - ~~Network~~
        - ~~UEFI Shell~~
    - Screen Resolution:
        - 1920x1080

- `airootfs/root/netboot.iso`
    - ESP Bootload iPXE binary from ISO


Since in UEFI mode we have no possibility to change the Boot Order. And saved parameters in `OVMF_VARS.fd` fails when MAC address change, we decided to use `Bootable iPXE ISO`, because CDROM is always first on Boot Order.


## Build the ISO
First clone `noVNC` git repository in `airootfs/opt/qemu-ressources/`
```bash
cd airootfs/opt/qemu-ressources
git clone https://github.com/novnc/noVNC.git --depth 1
```

Then build ISO image, go to project root and execute `build.sh`
```bash
sudo ./build.sh
```
## Change `root` password
Le mot de passe chiffré est ajouté avec la commande : `openssl passwd -6` et puis ajouter dans le fichier `airootfs/etc/shadow`
```
...
root:le_hash_de_mot_de_passe:14871::::::
...
```

## Set VNC Password
```bash
echo MYVNCPASSWORD | vncpasswd -f > airootfs/opt/qemu-ressources/x11vnc.pass
```
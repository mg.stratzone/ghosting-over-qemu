#!/usr/bin/env bash
# shellcheck disable=SC2034
if ! [ $(id -u) = 0 ]; then
   echo "The script need to be run as root." >&2
   exit 1
fi
rm -rf /tmp/archiso
mkdir -p /tmp/archiso/{work_dir,out_dir}
mkarchiso -A "Ghosting over QEMU" -P "Strategic Zone" -v -w /tmp/archiso/work_dir -o /tmp/archiso/out_dir ./